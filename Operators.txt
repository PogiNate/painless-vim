# Operators: Telling Vim to Change the Words

Okay, so,  you've got line numbers, colors, and a basic idea of how to get your cursor from place to place. If you wanted, you could stop here, move the cursor in normal mode, press `i` to go into insert mode, and make all your changes like you would in any other editor. But vim has a better idea. You can make a lot of the most common changes right from normal mode. For now we're going to focus on the three most common commands: Cut, copy, and paste.

Only they have different names in vim. Remember that vi[^veeeye] has been around since before the whole "clipboard" metaphor was ubiquitous. So the instead of 'cut, copy, paste' it's "delete, yank, put". The operators for these three commands are `d`, `y`, and `p`. Determining which command goes with which operator is left as an exercise for the reader. 

Q> ## Why "yank" ?
Q> 
Q> I don't know. [Nobody really knows][whence-yank]. My favorite theory is that all the useful mnemonics were taken, and "yank" kind of means the same thing as "pull", so they went with it. 
Q>
Q> It's worth noting that if you ever decide to learn emacs you'll be annoyed by the fact that "yank" in emacs is exactly the opposite of "yank" in vim: In vim "yank" means "copy". in emacs it means "paste". 

To play with these three commands, let's open up a sample document that we don't mind really *really* messing up. 

## Operators Alone

By themselves the commands each work on a single character. For example, if you had accidentally typed the word "ddoctor" you could position the cursor on the first d and press `d`. If you're in insert mode you'd get "dddoctor", but if you did it right in insert mode you'd get an appropriately spelled "doctor". 

Go ahead and play with these three commands for a while, but let's face it, by themselves they're not all that interesting. They get more useful when they are combined with *motions*. See? I told you there was a reason for calling them that!

## Operators and Motions



## Operating on an Entire Line {#OoaEL}

[^veeeye]:I'm only going to cover this once, because it's annoying. Fans of vi will insist that you pronounce "vi" like "vee eye" instead of like "vie" because shut up. "vim", thankfully, is pronounced "vim". This is yet another reason to never ever use non-improved vi.

[whence-yank]: http://english.stackexchange.com/questions/40657/how-yank-came-about-in-vi-and-emacs
