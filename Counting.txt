# Doing Things More Than Once

One of the great things about vim is that it has been built for the way programmers work. We often want to jump to a specific word or symbol in the text without moving there line by line, or we want to replace the next four words with four new words, or other such silliness. It's part of our job, and as I've said before, vim is all about making our jobs easier. It's good at it!

Part of how it makes our job easier is by letting us *repeat* our actions as many times as we need to accomplish what we're doing.  

##  Using Counts {#counts}

This is usually accomplished by typing a number before you give vim a command. For example, let's say you wanted to move the cursor four characters to the right. You could do this by typing `llll` or you could type `4l` and you just saved yourself *two whole keystrokes!* That's money in the bank, friend. But as useful as it is to save two whole keystrokes, you can apply this concept to a lot of other motions and actions as well. Open up `counts.txt` in the sample documents and find this line

{Line-numbers=on, starting-line-number=6}
~~~~~~~~
I am so incredibly stupid, useless, and talented.
~~~~~~~~

Now, reading this, you realize that you need to get rid of the words `stupid, useless and`. You could delete those three words individually by putting your cursor before a word and typing `dw`[^shortly] three times, but why waste the time? Instead, put your cursor before the word "stupid" and type `3dw`. BAM! Just like that your sentence is much more indicative of you as a person:

[^shortly]: Wondering what that `w` means? Good eye. We will be getting to that right after the aside. 

{Line-numbers=on, starting-line-number=6}
~~~~~~~~
I am so incredibly talented.
~~~~~~~~

A> ## The Exception to the Rule
A>
A> The "count + action" pattern can be used just about everywhere in vim, and is often the simplest way to do things. In general, if you hit [letter][number] it will do whatever [letter] does [number] times. We'll explore this pattern more in the following section. 
A>
A> But we need to talk about one useful exception to that pattern. You see, normally, when you hit `G` it will move your cursor to the end of the file. So you would think that `G13` would somehow move your cursor to the end of the file 13 times. But that's not a very useful command. So capital-G-plus-count behaves differently. Instead of jumping to the end of the file 13 times, it just jumps to line 13. As you would expect, `G42` would jump to line 42. So, remember the rule that I'm teaching in the rest of this chapter, but also remember this exception to that rule.  

## Using Text Objects

Sometimes you don't want to manually count how many characters you need to operate on. You want to delete the next word without counting how many letters are in it. Vim is here for you. 

We've already covered part of this in a sneaky way in the "[Operating on an Entire Line](#OoaL)". If you double an operator you're (usually) telling vim to apply it to the line as a whole. In this case, "the line as a whole" is what vim calls a *text object*. 

If you're a programmer you're probably already familiar with the concept of objects: they're the building blocks of most of the programming we've done since 1980. But I'm going to go ahead and explain objects anyway.

In vim terms, a text object is a set of characters. It can be a single character, a few characters, a word, an entire line…all the way up to the entire document. Clever person that you are, you've already been using text objects in the last section. When you tell vim `d5l` what you are telling it is "the five characters to the right of the cursor make up a text object. Delete it." If you told it `d5j` you'd be telling it that the next five *lines* constitute a text object whose presence is no longer required. But vim also gives us shortcuts for a number of other common text objects:

{title="Text Object Shortcuts"}
| Shortcut	| Text Object	|
|---------------|------------------------|
| w		| from the cursor to the end of the word |
| W		| from the cursor to the end of the WORD[^wordvsWORD] |
| s		| the current sentence |
| ' or "	| a string surrounded in quotes |
| l 		| from the cursor to  the end of the line |

There are many *many* others, but this will get us going. When you combine an *operator* with a *count* and a *motion* you have a *command* [^count_terminology]. 

A> ## "Commands" Don't Sound "Painless"!
A>
A> I understand the feeling. But think of it like this: When you tell someone to do something, you usually have a *noun*, a *verb* and a *location*, like "Bring the car around." In this sentence, *bring* is the verb, *the car* is the noun, and *around* is the location. 
A>
A> Vim commands are compact sentences telling vim what to do. The *operator* is the verb (things like "delete" or "copy"), the *text object* is the noun ("5 lines" or "current word") and the *motion* is the location. This syntax gives you the flexibility to make specific changes to the text quickly. 

For now let's practice a bit. Open the file `counts.txt` and go to line 9. There are four lines here that are just *begging* to be deleted. Let's fulfill their wish, with three keystrokes. Using the **operator-count-motion** syntax you can just type `d4j` and those lines are *gone*. 

Only they're not. You can use counts with `p` as well. Go down a few lines and paste them back two or three times. We're not sure when you'd want to paste multiple lines multiple times, but it's worth playing with at the very least.

## Behold The All-Powerful Dot

This particular trick doesn't have anything to do with counting, but it will be used with counted actions a *lot*.  The `.` character, that simplest of all characters, is one of your best friends in vim. It's meaning is simple: it repeats the last change you made to the text, but at your current position. 

A> ##What is a "Change"?
A>
A> A change is anything that alters the text between two normal mode commands. If you go into insert mode, add three empty lines, write two paragraphs, and then delete the first paragraph all before you go back into normal mode vim will call all of that one "change".  For this reason, vim folks like to suggest that you keep your changes small: edit a few characters or a line and then get back to normal mode as quickly as possible. This has two advantages: first, it means that the `u` key is more useful, because it will only undo a little bit of work instead of removing two paragraphs. And second, it makes your changes more *reusable*, as we're about to see. 
A> 
A> It's worth remembering that you can make changes *from* normal mode as well. Using `dd` to remove a line is a change, just as if you'd deleted the line from insert mode. The `.` command will redo normal mode changes just like it will insert mode changes.

In `counts.txt` there's a wonderfully contrived example: The simple "Jack and Jill" nursery rhyme, but somehow "Jill" got replaced with "Jack". Which means the first line reads

	Jack and Jack went up the hill to fetch a pail of water

That's a good trick, Jack! Let's fix up the second "Jack" using the change operator. Position your cursor after the letter J:
	
	J|ack

And type `cw` (that's "change everything from where the cursor is to the end of the word"). You should now have

	J|

And be in insert mode. Type `ill` and hit `<esc>`. Now that the second Jack is a Jill, you can read through the rest of the poem and find any other mistakes. On the third line it says

	And Jack came tumbling after.

Which makes no sense. Slap your cursor down like last time, but instead of typing `cw` you can just hit the period button and bam! Your last change is repeated with a single keystroke. If you've ever had to change all instances of a variable name in a file you can readily appreciate how useful this can be. It's especially great when combined with [searches](#searching), but more on that later.

The dot command is a good reason to be selective about what you do in insert mode, and how much you do between returns to normal mode.

[^wordvsWORD]: I'll get to the difference between a lower case word and an upper case WORD very soon. 

[^count_terminology]: There is a lot of discussion about what this is actually called. I'll let you dive into that fun world of terminology on your own some other time. I believe my nomenclature for things like *motions, counts, operators* and *commands* makes sense, and won't get you lost when you start diving into vim's [help system](#help).
