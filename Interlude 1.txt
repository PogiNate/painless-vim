# Interlude: The Joy of a Good Cheat Sheet {#interlude-one}

While you're learning vim it can be hard to remember all the commands and controls and letters and combinations and stuff, and sometimes no matter how much you ask your brain to pull up the one you need it's just not going to do it. 

Which is why it's a good idea to have someone help you out. A simple memory prompt hung up on the wall behind your computer can spur your brain back into activity. 

There are a lot of cheat sheets out there, most of which are the product of a lot of time and effort on the part of the creator, who was usually trying to create the "perfect" cheat sheet, meaning the one that works best for the way they think. 

In fact, since I first wrote this “interlude” I have created my own cheat sheet, which is included in your *Painless Vim* download. And hey, you spent the money on a vim book, you deserve a cheat sheet to go along with it. My cheat sheet is a distillation of all the commands and actions that I find myself using most frequently.

But if you’re overburdened with cash, or just don’t particularly care for the *Painless Vim* cheat sheet then read on, my friend. I won’t be offended, I promise.  

For my money [^money] the best one out there is the ["Beautiful" Vim Cheat Sheet](http://vimcheatsheet.com/), because it's clean, well laid out, and informative. When you start out you can just focus on the movement arrows that dominate the top left corner, and after a while you will start to see that the whole thing is packed with things you can use. 

There are a number of [other good choices](#cheat-sheets). The point of this interlude is to spend a little time finding one that makes sense to you. But the “Beautiful” one has earned itself a permanent spot on the wall behind my monitor. [^Until]

[^money]:And it actually cost me some cash money, but not a ton.

[^Until]: Until I created my own, that is. 