# Moving Around in Vim: Dance Steps

Now let's take everything we've learned about motion and give it a few last tweaks to make it really super useful. There are two text objects that look like they're the same thing but aren't. The difference between them can either cause you problems or make your life  better. These text objects are *words* and *WORDS*. 

## Words and WORDS

The easiest way to describe a WORD is that it's every *character* between one whitespace and the next. Thus `http://I.am.a.frog.com` is a single WORD, even though you and I would parse it as six words[^not-counting]. The lowercase "word" text object is any uninterrupted series of *letters*.  Thus `http://I.am.a.frog.com` is made up of six “word” objects, separated by punctuation marks. 

When you compose vim commands, you'll specify that you want to operate on a "WORD" with a `W` and a "word" with a `w`, as you might expect. If you want to play with the differences you can check out `motion3.txt`, and have a look at the exercises there. Using the url above, you could position the cursor at the beginning of the word `frog` and change just that word with the `cw` command. If you wanted to change the entire string into something else you could put your cursor at the beginning of the line and type `cW` instead. 

[^not-counting]: Or possibly five words and a protocol specification.

But always moving your cursor to the beginning of a text object to operate on it is a pain. Vim is smart, and can figure out where word boundaries are even if  your cursor is in the middle of the word. You just need to tell it what boundaries you're looking for. You can do that with the "inner" and "all" command modifiers.

## Inner and All

As you know, the text object commands `w s l` operate on the current text object (either a word, a sentence or a line, respectively. Check the [table](#text-object-table) if you need a quick refresher) from your cursor position to the end. But what if you've got a long word like "Omphaloskepsis[^omph-def]" and your cursor is somewhere around the first s? It's silly to have to move your cursor back to the initial `O` just to be able to delete the word, and vim has a much better way. 

You simply tell vim how to handle the *borders* of the text object in question and what to do with the text object. What constitutes a border depends on the kind of text object you’re using. A “WORD” is bordered by whitespace on both sides. A “word” is bordered by whatever isn’t a letter on either side. A sentence is bordered by a line ending or period. In general, vim works hard to match its concepts of borders to what humans think of as borders. 

If you tell vim to operate on an *inner* text object it will leave the borders alone. For example, if I do a `diw` on the word "Galt" in the sentence `Who is John Galt?` we are left with `Who is John ?` Which isn't grammatical, with that question mark floating around by itself at the end. However, if I were doing a `ciw` instead it would preserve the space between `John` and whatever new last name I want to give to Ayn Rand's character:
	
	Who is John Punchfist?

Much better.

However, when you're straight up deleting text, you often want to modify *all* of a text object, spare whitespace included. To do this change the `i` in the previous commands to an `a`. If I had said `daw` on John's last name in the example above the resulting sentence would have read `Who is John?` which is perfectly grammatical, if a bit vague, and nowhere near as awesome as asking who John Punchfist is. 

You can no doubt see why I wouldn't want to do `caw` there, because then I would have to *type* that space between "John" and "Punchfist" back in *all by myself*. Can you even *imagine*?

[^omph-def]: Contemplation of your navel as an aid to meditation. [Really.](http://en.wikipedia.org/wiki/Omphaloskepsis)

But `i` and `a` can do more than that. The "Inner" and "All" commands operate on just about any text object, including some fairly clever ones. Vim is smart enough to know that some symbols, like parentheses and quotation marks, wrap text objects that you may want to operate on as a whole. So it'll let you! For example, consider the following sentence:

	"Hello there! (He| lied)". Ted was always cheerful.

Not only is it terrible writing, it's terrible grammar. But it's a fun sentence to play with. Let's say we want to get rid of the whole `(he lied)` bit. Easy! We're going to tell vim to “Delete everything inside these parentheses, and take the parentheses with it.”  It's easier in vim speak, just type  `da(`

Just like that you've got 

	"Hello there! |". Ted was always cheerful.

Now let's say we want to change Ted's greeting. Since your cursor is already inside the quotation marks you can change the text between quotes and leave the quotes alone. `ci"` is your command of choice, and you can make Ted say whatever you want:

	"| ". Ted was always cheerful.

Vim is smart about these enclosures, and there are quite a few you can use. Here are a few of the more obvious ones:

{title="Enclosures"}
|Enclosure	| Meaning	|
|---------------|----------------|
| `{`	| Everything between two curly braces  |
|`[`	| Everything between two square brackets |
|`<`	| Everything between two angle brackets  |
|`t`	| Everything inside the closest XML or HTML tags  |

These all give you an easy way to modify exactly the text you want to change and nothing else. 

## Move by Counting

We've already seen the beginning of this concept in the [Using Counts](#counts) section, but let's flesh it out a bit here. Motion commands are still commands, and you can tell vim to repeat them just like anything else. This means that you can move five words right by simply typing `5w`, or you can move six lines down: `6j`. This is often a faster way to get where you're going than hitting `j j j j j j`.

But not always.  The reason for this is the human ability called *[subitizing](https://en.wikipedia.org/wiki/Subitizing)*. Simply put, we as humans can easily recognize groups of things up to about three or four without counting them, but once you get above four we slow down considerably. In vim terms this means that if you look at a sentence and see that you have to move three words right you will just hit `3w` without thinking about it. But if you have to move six words right you're probably counting those words, then entering the command, and that's often slower than just mashing the `w` key six times, or (even better) just searching for the exact spot where you need to make your changes.  More on that  coming [soon](#searching). 

##(Book)marks

Every once in a while you want to get from place to place quickly and easily without counts, without searches, without doing anything but telling vim where you would like to go, thank you so very much. Kind of like a bookmark in your browser, but for a text file. That's what *marks* are for. 

A mark is just a pointer to a line in a file. If you set a mark at line seven of a file that's where it will stay, giving you an easy way to return to line seven. But vim wouldn't leave anything that boring or underdeveloped.

A> Just a quick note: Vim calls these "marks" not "bookmarks" for the good and proper reason that vim is an editor, not a book. Just so you know. Okay, as you were.

The marks are roughly divided into two groups: *lowercase* marks and *uppercase*, or*file*, marks. The obvious difference between the two is how you refer to the mark. The important difference is that lowercase marks are limited to lines inside a  single file, while uppercase marks can jump freely to any line of *any* file. The two types of marks work in much the same way with slight differences. Let's look at how to use lowercase marks first.

To set a mark you type `m` followed by a letter in normal mode. That's all there is to it! Unless of course you want to get *back* to that mark later. Then there's a little more. There are two ways to return to a mark. If you want to return to the exact character on the exact line where you set the mark you type a back tick (usually the same key as ~, right next to the number 1 on your keyboard) and the letter. So if you set mark `a` on line 5 you could then go anywhere else you want in the file and jump back to line 5 by typing

	`a

If you only want to get to the *line* where you set the mark  you can use a straight single quote (same key as double quotes, next to enter) and the letter. To get back to line 5 at the beginning you can just type 

	'a

And you're good. But wait, there's more! (There's always more.) Whenever you jump to a mark, vim remembers where you jumped *from*, and stores it as a special mark `’` . So you can jump to line 5 with your `a` mark, make your change, and then just hit the single-quote key twice and you're back where you started.

Naturally, you can do this with any letter from `a` to `z`, meaning you've got twenty-six marks you can set in any file you're working on. But what about when you're working on a bunch of files? That's where the uppercase marks come in. 

Uppercase marks behave the same way as lowercase marks, with one notable exception: they allow you to jump from one file to another with the greatest of ease. Like the lowercase marks, uppercase marks are set on a specific character of the file, meaning not only can you get from `sample.c` to `sample.h` in a single motion, you can get to line 471 of `sample.h` from anywhere with a single motion. But as much fun as it is to write c code, let's talk about an uppercase mark that is instantly useful to every vim user everywhere.  This is an exercise that you'll definitely want to do.

- Open your `.vimrc` file in vim.  
- Type `mV`
- Open any other file in vim.
- Type `'V`

Boom! You can now get to your `.vimrc` configuration file from anywhere at any time. While it's true that you will eventually stop playing around with your configuration on a daily basis, when you're first learning and configuring vim to work for you instead of against you this particular mark can save you a lot of time. 