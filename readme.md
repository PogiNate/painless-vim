# Painless Vim

This is the text for the in-progress book *Painless Vim*. While this book is being written out in the open, please note that all text is still 

***Copyright 2013-2014 Nate Dickson*** 

and may not be sold or distributed without permission. 

That said, if you are interested in looking over this text, feel free to read it over. If you are here because you purchased the book on [leanpub](https://leanpub.com/painless_vim) and want to make some suggestions, know that I love you dearly and I'm so grateful for your patronage.

## How to Submit Changes

As you can see, the text in this directory is just a bunch of text files. This is the directory from which lean pub builds the book, and that is how they like it. see `book.txt` for the order in which the files are integrated, and things will make a little more sense.

To submit a change, you can create a pull request with your changes marked with [Critic Markup](http://criticmarkup.com/). I will pull in the file, look through your changes and make my decisions accordingly. If you just make the changes to the file and submit that I will still accept the changes, but it'll take me a little longer to go through it with a diff tool. 

You can also use the feedback tool on Leanpub or the issue tracker on this page to submit your comments and changes. however you submit feedback please know that I'm insanely grateful for your time and effort! It's people like you that make online web publishing the awesome and revolutionary thing it is.