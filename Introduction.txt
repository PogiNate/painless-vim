# Introduction

## Why Learn Vim
Vim, for all its eccentricities, is designed to be *fast* and *developer friendly*. Smart people have been using and improving it for decades and it's gotten to the point that it's pretty darn good in the right hands. As a former vim critic who has started using it, I can tell you that it is *that* fast when you get going. You can make a surprising number of changes quickly and efficiently in vim, and once you start navigating, making changes, and issuing commands using  just the home row keys using anything else will feel clunky by comparison.

### It is *that* fast
Vim, used well, is a speed *demon*. Once you get used to the slightly weird controls every other text editor will feel unwieldy. Vim was designed to keep your hands where they belong, without using the mouse or the arrow keys.

Now, like I said before, if you want to use the mouse and arrow keys while you get used to vim, go right ahead! You're not being productive if all you're doing is trying to remember how to edit the third character in the line below your cursor. But the *promise* is that eventually you'll get comfortable with the vim way of doing things and be much happier. There are other reasons, too:

### Vim *commands* get around

A lot of unix-y things use the vim keybindings for moving around. Even web services like gmail use the `h j k l` magic keys to move between emails. If you get comfortable with vim idioms you'll be more comfortable with a lot of other tools as well. 

### Vim *itself* gets around

Let's face it, vim is *everywhere*. If you install git on windows vim comes along for the ride. If you have a Linux box or a Mac you've already got vim.  And if you're like me[^1] it's always slightly bugged you that there was this editor on your machine that you don't like or know how to use. If you get comfortable with vim then you don't have to panic when you have to enter a commit message from the command line in git. 

## What This Book Is

This book aims to be: 

### Short
You've got better things to do than read books about editors. The main goal of this book is to get you comfortable with the editor and feeling like you're ready to move on, bending vim to your wishes. If you're like me, you get annoyed at making changes someone else forced on you after a while, and you just want to strike out and do it yourself. I'll let you do just that.

### Cheap
Vim is free. While I don't want to sell my work for nothing, I'm coming pretty darn close. This is a beginner's book, and my goal is to lower the bar to entry. To start with, I'll price it low and hope that helps you along.

### Easy to understand
Vim is complex; this book shouldn't be. My main goal here is to help you build confidence and get your feet under you. If you're reading this during the beta period and you've got suggestions, please send them on over to me! I'm more than willing to listen and change. 

### Friendly!
My high horse died a while back, and I never bothered to buy myself a soapbox, so I won't be doing any preaching. Remember that friendly face in the hotel who shows you all the great spots in town? That's this book. I'm not here to judge. 

### Opinionated (but in a good way)
I'll keep the tone friendly, but I got *opinions* people. There are things I think are good and things I think aren't so good. But I'll let you know *why* I think some things are good or not so good and let you make up your mind on your own. 

### Lighthearted, but serious
The *tone* of the book is meant to be light and approachable, but the *advice* is meant to be rock solid. I'm not going to have you do things to your configuration files that I wouldn't do, even temporarily, or to prove a point. The goal is to make it easy for you to get things *right* [^2] the first time. 

## What This Book Isn't

This book definitely is *not*:

### The end-all-be-all reference on vim

I'm here to help you get started, not to be the only book you'll ever buy on the subject. The main reason for this isn't laziness, thank you very much. Well, okay, it kind of is. There are a *ton* of books, websites, wikis, and other documentation out there that will help you along, once you get over the initial learning curve. I'm here to help you over that first curve. 

### A list of tips

A lot of the stuff in the later chapters won't make a ton of sense if you haven't been through the earlier chapters. The chapters are laid out iteratively, starting with small changes that build up to a final configuration that should work well. Then you'll have a good jumping-off point for your further modifications. 

### A deep dive

The goal here is to give you enough information to be comfortable, but not all the underlying technologies and line editor commands and other such things. Again, these are available online and in many other reference guides all over the place. When you decide that you want to know more about something you'll be able to find it. I have faith in you.

## Who am I?
I'm Nate Dickson, it says so right on the cover.

But beyond that, I'm a professional programmer by day, blogger, writer, and daddy by night. You can check out a fairly large amount of my writing on my personal blog[^3], my less-maintained Apple blog[^4], or a blog I used to contribute to[^5]. You can also check me out on Twitter[^6], Github[^7] and App.net[^8]. 

In terms of my vim credentials, I don't have many. I use vim in my day job and I'm a command line junky, with the `.zshrc` and `.tmux.conf` files to prove it. 

But my quasi newbie status is part of why I'm writing this book right now. I've spent the last few months combing blogs and websites, buying or borrowing books on vim and grabbing onto the arm of anyone I know who uses vim and begging them to help me fix whatever mess I've most recently gotten myself into. Now that I'm fairly comfortable, I'd like to pass what I've learned along to ease the learning curve for everyone else.

## References

I will refer you to a lot of other books, posters, blogs, wikis, etc. etc. over the course of this book. For the most part these are things I've used extensively myself, and can heartily recommend. Again, I'll try to provide reasons for my references and let you decide if it sounds like something you'd like to investigate on your own.

## Sample Documents

Throughout the book there will be exercises to try out. For convenience, I have created a repository of sample documents for use with these exercises. They are stored on [Github](https://github.com/PogiNate/Painless-Vim-Samples), and the readme file has instructions for how to get them in place, if you're not familiar with git and Github.

## The Obligatory "Conventions Used in This Book" Section

In general, I use the same conventions everyone else uses. Code appears in `a monospaced font`, and longer blocks of code will appear

	in a large block
	of monospaced fonts.

Special keys like `<shift>`, `<enter>`, `<esc`>, will have angle brackets around the name to make it real obvious that you shouldn't be typing that word, you should be pressing that key.  If you need to press `<ctrl>` and another key at the same time (and you *will* need to do this, quite frequently) I'll just write them like so: `<ctrl>d`.

Vim uses both upper- and lower-case characters, so remember that "`S`" is `<shift>s` and "s" is just `s`. 

Those are the only two times you should ever need to press two keys at the same time: either to hit `<ctrl>` and a character, or to press `<shift>` and a character to make something upper-case. 

Which is good, because there will be a lot of times where you have to hit multiple characters in sequence. For example, let's say you need to delete the next character after your cursor. You would press `d` (for delete) and then `l` (for the next character. Don't worry about it now; we'll get into it later). When I write this out in the book I will just write the two letters in a row, like so :

	dl

I realize that looks exactly the same as my "type them at the same time" notation. Just remember that you'll only ever need to press `<ctrl>` or `<shift>` at the same time as another character. In all other situations, two characters in sequence means type them one after the other.

Okay, got it? It's pretty natural, I promise. Let's move on.

From time to time I will need to show you *exactly* where your cursor needs to be when you perform a command. In these cases I will put a pipe symbol where the cursor resides, like so:

	The cursor is in the middle of the word pi|pe

Again, this will all make more sense in a bit. For now let's get moving. 

[^1]:	A few months ago

[^2]:	According to me, see the whole "Opinionated" thing up above.

[^3]:	[http://natedickson.com](http://natedickson.com)

[^4]:	[http://crazyapplenews.com](http://crazyapplenews.com)

[^5]:	[http://favoritethingever.com](http://www.favoritethingever.com/author/nate-dickson/)

[^6]:	[@poginate](https://twitter.com/poginate)

[^7]:	[http://github.com/PogiNate](http://github.com/PogiNate)

[^8]:	[@poginate](https://alpha.app.net/poginate). Yeah, I see the pattern too.

