# Interlude: Reviewing Views {#interlude-3}

It's worth taking a few moments here to go back over the hierarchy of things that show up in a vim session, just to get them all straight in our minds and to take a breather.  The quick rundown looks like this:

1. A *file* is a bunch of data on a disk.
1. When you tell vim to edit a file, it creates a *buffer* of all the data from that file and lets you edit that.
1. A *window* lets you view the text in a buffer. You can have multiple windows open to different parts of the same buffer, if you so desire, or you can have multiple windows looking at different buffers on the same screen.
1. A *tab* (or, more accurately, *tab page*) is a collection of all the windows you can see at the same time. You can have a bunch of tabs open, and it's super simple to switch between them. 

Let's work from the bottom up, and just for fun we'll do this in a semi-Q&A format.

## Files

**Q:** So, vim doesn't edit files?

**A:** No. When you open a file in vim it is copied into a *buffer*, which is what you edit. Interestingly, the buffer is saved off at intervals into a *swap file*, which is there to keep you from losing your work. But we're going to ignore that for a minute. Vim's buffer is entirely in memory, and is only written back to the actual disk when you give vim the `:write` command. 

**Q:** Can you have multiple buffers for the same file?

**A:** vim works hard to prevent this from happening. When vim opens a file it creates the aforementioned swap file. If you open another copy of vim and tell it to edit the same file it will see the existing swap file and get very nervous. Before it lets you start editing the file it will ask if you want to make a copy, or perhaps open the file in read-only mode, basically do anything *other* than have two copies of vim fighting over the swap file. Having multiple authors on a single file is a task best handled by software specifically designed with that in mind, and vim isn't that software.

When you save a document vim copies the contents of the swap file into the current file. When you close vim it deletes the swap file. I cover this in a little more detail in the [next chapter](#files-vs-buffers) so you can skip ahead if you like. 

**Q:** So why does the swap file even exist? 

**A:** Because things don't always happen perfectly. The swap file exists for occasions where vim (or your computer) crashes without saving the changes you made to the buffer back into the file. Which is part of why you only have one swap file per file: it keeps the history clean and makes it easy to restore your changes.

That said, you can have more than one view into the same buffer, by having that buffer open in multiple *windows*.

## Windows

**Q:** What's the difference between a buffer and a window?

**A:** That's easier to show than tell. Do the following: 

1. Open vim without specifying a file.
1. Type `:sp` to split your screen into two windows
1. Start typing in either window.

You should see your text showing up in the other window. Both windows are looking at the same buffer, so your changes are reflected in both places. Having multiple windows into a single buffer is useful for reasons other than boring party tricks. You can have one window showing a method you're referencing and the other showing the method you're writing, for example. But windows are usually better used when they're looking at different buffers.

**Q:** How many windows can you have on a screen at once?

**A:** That depends on how big your screen is. If I have more than three open I suggest opening a new *tab*. 

## Tabs

**Q:** Okay then, how many *tabs* can I have open at once?

**A:** By default you can have ten tabs. I don't know why. I *also* don't know why you would want to *change* this number, but if you're so inclined you can add a statement like `set tabpagemax=15` to your `.vimrc` file and then you can have 15 tabs open. It's up to you. If you're working on 15 sets of files at once then you're far better at multitasking than I am.

**Q:** Why did you make a huge deal about the difference between *tabs* and *tabpages* earlier, but now you're just calling them all *tabs*?

**A:** Because *tab* is way shorter than *tabpage*, both to type and to say. Once you got the idea that you weren't getting a one-tab-to-one-file correspondence like you're probably used to from your browser we could settle down and simplify the language. It's true, however, that a lot of people will still use vim to open a tab for every file they're editing, and that's fine. I just wanted to remind you that you can do more with a vim tab than you can with a browser tab.

**Q:** Is there an easy way to open all the files you're working on in separate tabs?

**A:** I don't know about "easy" but if you want to open vim with a bunch of tabs you can call it like so:

	vim -p file1 file2 file3

And you'll have your files all in their own little tabs. I guess if you have a good programmatic way to build that command that would be "easy", but I generally just use NERDTree to open files from within vim.


**Q:** Why are you so against using a tab for a single file --or *buffer*, if you insist-- and so adamant that you need to have multiple windows open on each tab?

**A:** I'm not saying that you have to have multiple windows on each tab; I often use tabs for a single buffer. I just want people to remember that you've got more options and not get stuck thinking that one tab always equals one buffer. 

The other pitfall comes with plugins like NERDTree. People get used to having their file explorer open on the side of the screen, and having it be persistent across all tabs. Vim doesn't work like that. You can *make* it work like that with scripting and whatnot, but by default that's not how things work in vimworld. Just remember that all the windows you can see exist on a tabpage, and you'll be fine. 